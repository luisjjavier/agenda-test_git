﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Agenda.Common.Models;


namespace Agenda.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IContactosService
    {

        [OperationContract]
        Contactos GetContactosById(int id);
        [OperationContract]
        List<Contactos> GetContactos();
        [OperationContract]
        bool Update(int id, Contactos con);
        [OperationContract]
        int Save(Contactos con);

        
    }


    
    
}

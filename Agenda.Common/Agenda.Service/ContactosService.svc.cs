﻿using Agenda.Library.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Agenda.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IContactosService
    {

        public Common.Models.Contactos GetContactosById(int id)
        {
            return ContactosHelper.GetContactosById(id);
        }

        public List<Common.Models.Contactos> GetContactos()
        {
            return ContactosHelper.GetContactos();
        }

        public bool Update(int id, Common.Models.Contactos con)
        {
            return ContactosHelper.Update(id, con);
        }

        public int Save(Common.Models.Contactos con)
        {
            return ContactosHelper.Save(con);
        }
    }
}

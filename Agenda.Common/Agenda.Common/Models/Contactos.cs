﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Common.Models
{
    public class Contactos
    {
        public int Id { get; set; }

        [StringLength(100, ErrorMessage = "No pude ser tan grande")]
        public string Name { get; set; }
        public string LastName { get; set; }
    
        public int Age { get; set; }
        public string Address { get; set; }
        [Required]
        [Display(Name="Numero de telefono")]
        public string PhoneNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agenda.Common.Models;
using Agenda.Entity;

namespace Agenda.Library.Helper
{
    public static class ContactosHelper
    {
        public static Contactos ToModel (this TBL_CONTACTOS tbl)
        {
            return new Contactos() 
            {
                Address = tbl.Cts_V_Address,
                Age = tbl.Cts_I_Age,
                LastName = tbl.Cts_V_LastName,
                Name = tbl.Cts_V_Name,
                PhoneNumber = tbl.Cts_V_PhoneNumber,
                Id = tbl.Cts_I_Id
            };
        }

        public static TBL_CONTACTOS ToTable(this Contactos model)
        {
            return new TBL_CONTACTOS()
            {
                Cts_V_PhoneNumber = model.PhoneNumber,
                Cts_V_LastName = model.LastName,
                Cts_I_Age = model.Age,
                Cts_V_Address = model.Address,
                Cts_I_Id = model.Id,
                Cts_V_Name = model.Name

            };
        }
        public static List<Contactos> GetContactos()
        {
            List<Contactos> contactos = new List<Contactos>();
            using (ContactosEntities context = new ContactosEntities())
            {
                var rs = context.TBL_CONTACTOS.ToList();
                contactos = rs.Select(ToModel).ToList();
            }
            return contactos;
        }
        public static int Save (Contactos con)
        {
             TBL_CONTACTOS tbl = con.ToTable();
             using (ContactosEntities context = new ContactosEntities())
             {
                 context.TBL_CONTACTOS.Add(tbl);
                 context.SaveChanges();
             }
             return tbl.Cts_I_Id;
        }
        public static bool Update( int id ,Contactos con)
        {
            bool result = false;
            using (ContactosEntities context = new ContactosEntities())
            {
                var rs =context.TBL_CONTACTOS.SingleOrDefault(x => x.Cts_I_Id == id);
                if (rs!=null)
                {
                    rs.Cts_I_Age = con.Age;
                    rs.Cts_V_Address = con.Address;
                    rs.Cts_V_Name = con.Name;
                    rs.Cts_V_PhoneNumber = con.PhoneNumber;
                    context.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
        public static Contactos GetContactosById(int id)
        {
            Contactos result = new Contactos() ;
            using (ContactosEntities context = new ContactosEntities())
            {
                var rs = context.TBL_CONTACTOS.SingleOrDefault(x => x.Cts_I_Id == id);
                if (rs !=null)
                {
                    result = rs.ToModel();
                }
            }
            return result;
        }
    }
}

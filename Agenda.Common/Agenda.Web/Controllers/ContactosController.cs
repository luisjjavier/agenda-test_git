﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agenda.Web.Controllers
{
    public class ContactosController : Controller
    {
        //
        // GET: /Contactos/

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(ContactosService.Contactos con)
        {
            ContactosService.ContactosServiceClient service = new ContactosService.ContactosServiceClient();
            var Id= service.Save(con);
            return RedirectToAction("index");
        }
        public ActionResult Edit(int id)
        {

            ContactosService.ContactosServiceClient service = new ContactosService.ContactosServiceClient();
            var contacto = service.GetContactosById(id);
            return View(contacto);
 
        }
        [HttpPost]
        public ActionResult Edit(int id, ContactosService.Contactos con)
    {

        ContactosService.ContactosServiceClient service = new ContactosService.ContactosServiceClient();
        var Id = service.Update(id,con);
        return  RedirectToAction("index");
        }
        public ActionResult Index()
        {
            ContactosService.ContactosServiceClient service = new ContactosService.ContactosServiceClient();
            
            return View(service.GetContactos());
        }

    }
}
